﻿using System;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;

namespace FileSystem.Utilities
{
    /// <summary>
    /// A class to encapsulate a file system file or directory path and allow for easy comparison, appending and traversal.
    /// I originally wrote this utility class way back in 2005 (https://www.codeproject.com/Articles/10265/FileSystem-path-string-wrapper-class)
    /// in C++ and now that I'm back working in .NET land have decided to port it.
    /// </summary>
    public partial class Path
    {
        private string _drive = string.Empty;
        private string _path = string.Empty;
        private char _pathSeparator = System.IO.Path.DirectorySeparatorChar;

        private Lazy<bool> _isLocal;
        private Lazy<bool> _isUnc;
        private Lazy<bool> _isUrl;
        private Lazy<bool> _isRelative;
        private Lazy<Path> _localPath;
        private Lazy<Path> _parentPath;
        private Lazy<string> _baseName;
        private Lazy<string> _baseTitle;
        private Lazy<string> _baseExtension;
        private Lazy<uint> _depth;

        /// <summary>
        /// The path separator.  By default this is <see cref="System.IO.Path.DirectorySeparatorChar"/> but you can update it.  Valid characters are currently limited to <see cref="System.IO.Path.DirectorySeparatorChar"/> and <see cref="System.IO.Path.AltDirectorySeparatorChar"/>
        /// </summary>
        public char PathSeparator { get => this._pathSeparator; set => SetPathSeparator(value); }

        /// <summary>
        /// Create a path from the specified string.  This ctor assumes that the <see cref="System.IO.Path.DirectorySeparatorChar"/> separator is used.
        /// </summary>
        /// <param name="path">The path string</param>
        public Path(string path)
        {
            InitialiseWithPath(path, System.IO.Path.DirectorySeparatorChar);
            InitialiseLazies();
        }

        /// <summary>
        /// <para>Create a path from the specified string using the specified path separator character.</para>
        /// <para><b>Hint:</b> Use the <see cref="StringExtensionsForPath.GetPathSeparator(string)"/> extension method to try and determine the appropriate value for the <paramref name="pathSeparator"/> value.</para>
        /// </summary>
        /// <param name="path">The path string</param>
        /// <param name="pathSeparator">
        ///     <para>The path separator</para>
        ///     <para><b>Hint:</b> Use the <see cref="StringExtensionsForPath.GetPathSeparator(string)"/> extension method to try and determine the appropriate value for the <paramref name="pathSeparator"/> value.</para>
        /// </param>
        public Path(string path, char pathSeparator)
        {
            InitialiseWithPath(path, pathSeparator);
            InitialiseLazies();
        }

        /// <summary>
        /// copy ctor
        /// </summary>
        /// <param name="rhs">The path to clone</param>
        public Path(Path rhs)
            : this(rhs._drive, rhs._path, rhs._pathSeparator)
        {
        }

        /// <summary>
        /// Create a path from a known drive and drive (with a specific separator)
        /// </summary>
        /// <param name="drive"></param>
        /// <param name="path"></param>
        /// <param name="pathSeparator"></param>
        private Path(string drive, string path, char pathSeparator)
        {
            this._pathSeparator = pathSeparator;
            this._path = path;
            this._drive = drive;

            // still need to configure the lazies though!
            InitialiseLazies();
        }

        /// <summary>
        /// Return a path objec to the users "home" directory from the system environment variables.
        /// </summary>
        /// <returns>The path to the HOME directory</returns>
	    public static Path HomeDirectory()
        {
            // use getenv() function to retrieve HOME path
            var home = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile);

            // create path object from HOME
            return new Path(string.IsNullOrWhiteSpace(home) ? string.Empty : home);
        }

        /// <summary>
        /// Return a path object to the "current directory"
        /// </summary>
        /// <returns>The path to the current directory</returns>
        public static Path CurrentDirectory()
        {
            var cwd = System.IO.Directory.GetCurrentDirectory();

            // create path object from cwd
            return new Path(string.IsNullOrWhiteSpace(cwd) ? string.Empty : cwd);
        }

        /// <summary>
        /// Get the "clean" version of the the current path by resolving:
        /// <list type="bullet">
        ///     <item><b>..</b></item>
        ///     <item><b>.</b></item>
        ///     <item><b>~</b></item>
        /// </list>
        /// </summary>
        public Path Clean
        {
            get
            {
                // compress the path, and resolve ~
                bool hasLeadingPS = this.HasLeadingPathSeparator;
                bool hasTrailingPS = this.HasTrailingPathSeparator;

                // split path into it tokens, 
                var tokens = this._path.Split(this.PathSeparator);
                var retValue = new Path(string.Empty, this.PathSeparator);

                retValue._path = hasLeadingPS ? $"{this.PathSeparator}" : string.Empty;
                retValue._drive = string.Empty;

                var home = HomeDirectory();
                var cwd = CurrentDirectory();

                for (int i = 0; i < tokens.Length; i++)
                {
                    var token = tokens[i];

                    if (token == "~"            // check for ~
                        && i == 0                   // check that the ~ is the first character in the path
                        && home.IsValid)           // check that we have been able to resolve the home path
                    {
                        // copy path					
                        retValue._path = home._path;

                        // copy drive
                        retValue._drive = home._drive;
                    }
                    else if (token == "."           // check for .
                            && i == 0			    // check that the . is the first character in the path
                            && cwd.IsValid)        // check that we have a valid current working directory
                    {
                        // copy path
                        retValue._path = cwd._path;

                        // drive letter must be updated
                        retValue._drive = cwd._drive;
                    }
                    else if (token == ".."
                            && retValue.Parent.IsValid)
                    {
                        // update path to parent path.
                        retValue._path = retValue.Parent._path;
                    }
                    else
                    {
                        // add path separator on the end if needed.
                        if (retValue._path.Length > 0 && retValue._path[retValue._path.Length - 1] != this.PathSeparator)
                        {
                            retValue._path += this.PathSeparator;
                        }

                        retValue._path += token;
                    }

                    // we're adjusting the internals of the retValue path object
                    // so we MUST also initialise the lazies again.
                    retValue.InitialiseLazies();
                }

                // put / on end as required
                if (hasTrailingPS && (retValue._path.Length > 0 ? (retValue._path[retValue._path.Length - 1] != this.PathSeparator) : true))
                {
                    retValue._path += this.PathSeparator;

                    // we've just adjusted the internals of the retValue path object
                    // so we MUST also initialise the lazies again.
                    retValue.InitialiseLazies();
                }

                return retValue;
            }
        }

        /// <summary>
        /// Returnes the drive letter of the path.  If the path has no drive letter then an empty string will be returned.
        /// </summary>
        public string Drive => this._drive.Length >= 2 ? $"{this._drive[0]}" : string.Empty;

        /// <summary>
        /// Is the path object value?
        /// </summary>
	    public bool IsValid => this._path.Length > 0;

        /// <summary>
        /// Return the total number of characters in the path
        /// </summary>
	    public ulong Length => (ulong)(this.ToString()?.Length ?? 0);

        /// <summary>
        /// Is the path a UNC file path?
        /// </summary>
        public bool IsUnc => this._isUnc?.Value ?? false;

        /// <summary>
        /// Is the path a "local" file path?
        /// </summary>
        public bool IsLocal => this._isLocal?.Value ?? false;

        /// <summary>
        /// Is the path a Url?
        /// </summary>
        public bool IsUrl => this._isUrl?.Value ?? false;

        /// <summary>
        /// Is the path relative?
        /// </summary>
        public bool IsRelative => this._isRelative?.Value ?? false;

        /// <summary>
        /// Return the parent path object of the current path
        /// </summary>
        public Path Parent => this._parentPath?.Value ?? CreateEmptyPath();

        /// <summary>
        /// Returns the name of the lastmost path name in the path INCLUDING any trailing file extension (if one exists)
        /// </summary>
	    public string BaseName => this._baseName?.Value ?? string.Empty;

        /// <summary>
        /// Returns the name of the lastmost path name in the path EXCLUDING any trailing file extension (if one exists)
        /// </summary>
	    public string BaseTitle => this._baseTitle?.Value ?? string.Empty;
 
        /// <summary>
        /// Returns the extension (without the .) of the lastmost path name in the path(if one exists)
        /// </summary>
	    public string BaseExtension => this._baseExtension?.Value ?? string.Empty;

        /// <summary>
        /// Is the path the root path (one that was constructed with a path separator at the start).
        /// <code>eg. c:\ or /</code>
        /// </summary>
        public bool IsRoot => this._path.Length == 1 && this._path[0] == this.PathSeparator;

        /// <summary>
        /// If the path is a root path, then return the root.
        /// Otherwise return an Empty path
        /// </summary>
	    public Path Root => new Path((this._drive.Length > 0 ? (this._drive + this.PathSeparator) : this.HasLeadingPathSeparator ? $"{this.PathSeparator}" : string.Empty), this.PathSeparator);

        /// <summary>
        /// <para>
        /// Return how deep the path is.  
        /// </para>
        /// <b>Note:</b>
        /// <list type="bullet">
        /// <item>If there is a  path separator at the start of the path: The root path (/) is considered to be at depth 0</item>
        /// <item>If there is NO path separator at the start of the path: The first named path is considerd to be at depth 0</item>
        /// </list>
        /// </summary>
	    public uint Depth => this._depth?.Value ?? 0;

        /// <summary>
        /// Return a copy of this path as a "local" os path (native file system, unc or url) using the OS's native path separator
        /// </summary>
        public Path LocalPath => this._localPath.Value ?? CreateEmptyPath();

        /// <summary>
        /// Does the path have a leading path separator.
        /// </summary>
        public bool HasLeadingPathSeparator => this._path.Length > 0 && this._path[0] == this.PathSeparator;

        /// <summary>
        /// Does the path have a trailing path separator
        /// </summary>
        public bool HasTrailingPathSeparator => this._path.Length > 0 && this._path[this._path.Length - 1] == this.PathSeparator;

        /// <summary>
        /// Return a copy of this path <b>WITH</b> a trailing path separator
        /// </summary>
        public Path WithTrailingPathSeparator => this.HasTrailingPathSeparator == true 
                                                    ? new Path(this) 
                                                    : new Path(this._drive, this._path + this.PathSeparator, this.PathSeparator);

        /// <summary>
        /// Return a copy of this path <b>WITHOUT</b> a trailing path separator
        /// </summary>
        public Path WithoutTrailingPathSeparator => this.HasTrailingPathSeparator == true 
                                                        ? new Path(this._drive, this._path.Substring(0, this._path.Length - 1), this.PathSeparator) 
                                                        : new Path(this);

        /// <summary>
        /// Return a copy of this path <b>WITH</b> a trailing path separator
        /// </summary>
        public Path WithLeadingPathSeparator => this.HasLeadingPathSeparator == true
                                                    ? new Path(this)
                                                    : new Path(this._drive, this.PathSeparator + this._path, this.PathSeparator);

        /// <summary>
        /// Return a copy of this path <b>WITHOUT</b> a leading path separator
        /// </summary>
        public Path WithoutLeadingPathSeparator => this.HasLeadingPathSeparator == true
                                                    ? new Path(this._drive, this._path.Substring(1), this.PathSeparator)
                                                    : new Path(this);

        /// <summary>
        /// Is this path a parent of the specified child path?
        /// </summary>
        /// <param name="child">The child path</param>
        /// <returns>True if this path is a descendent of the parent, otherwise false</returns>
        public bool IsParentOf(Path child)
        {
            return child.GetParentAtDepth(this.Depth) == this;
        }

        /// <summary>
        /// Is this path a child of the specified parent path?
        /// </summary>
        /// <param name="parent">The parent path</param>
        /// <returns>True if this path is a descendent of the parent, otherwise false</returns>
        public bool IsChildOf(Path parent)
        {
            return GetParentAtDepth(parent.Depth) == parent;
        }

        /// <summary>
        /// Is this path a parent of the specified child path?
        /// </summary>
        /// <param name="child">The child path</param>
        /// <returns>True if this path is an ancestor of the child, otherwise false</returns>
        public bool IsParentOf(string child)
        {
            var childPath = new Path(child, this.PathSeparator);
            return childPath.GetParentAtDepth(this.Depth) == this;
        }

        /// <summary>
        /// Is this path a child of the specified parent path?
        /// </summary>
        /// <param name="parent">The parent path</param>
        /// <returns>True if this path is a descendent of the parent, otherwise false</returns>
        public bool IsChildOf(string parent)
        {
            var parentPath = new Path(parent, this.PathSeparator);
            return GetParentAtDepth(parentPath.Depth) == parent;
        }

        /// <summary>
        /// Equality operator
        /// </summary>
        /// <param name="lhs">The path</param>
        /// <param name="rhs">The path to compare.  Must use the same path separator as the lhs</param>
        /// <returns></returns>
	    public static bool operator == (Path lhs, Path rhs)
        {
            // use case sensitive Equals
            return lhs.Equals(rhs, false);
        }

        /// <summary>
        /// Not Equal operator
        /// </summary>
        /// <param name="lhs">The path</param>
        /// <param name="rhs">The path to compare</param>
        /// <returns></returns>
	    public static bool operator != (Path lhs, Path rhs)
        {
            return !(lhs == rhs);
        }

        /// <summary>
        /// Add 2 paths together
        /// </summary>
        /// <param name="lhs">The path</param>
        /// <param name="rhs">The path.  Must use the same path separator as the lhs</param>
        /// <returns></returns>
        public static Path operator + (Path lhs, Path rhs)
        {
            if (!lhs.IsValid && !rhs.IsValid)
            {
                // return copy of this invalid path.
                return new Path(string.Empty, lhs.PathSeparator);
            }

            if (lhs.IsValid && !rhs.IsValid)
            {
                // return copy of this valid path
                return new Path(lhs);
            }

            if (!lhs.IsValid && rhs.IsValid)
            {
                // return copy of the rhs valid path.
                return new Path(rhs);
            }

            if (lhs.PathSeparator != rhs.PathSeparator)
            {
                // mismatching path separators!
                // return copy of lhs path
                return new Path(lhs);
            }

            // make sure rhs does NOT have a drive letter
            if (!string.IsNullOrEmpty(rhs.Drive))
            {
                // rhs has a drive letter, you can't add it to the lhs if it does!
                // return copy of lhs path
                return new Path(lhs);
            }

            // both paths are valid...
            string[] tmpStr =
            {
                lhs._drive + lhs._path,
                rhs._path
            };

            // make sure lhs has a / on the end.
            if (tmpStr[0][tmpStr[0].Length - 1] != lhs.PathSeparator)
            {
                tmpStr[0] += lhs.PathSeparator;
            }

            // make sure rhs path has NOT got a / at the start.
            if (tmpStr[1][0] == lhs.PathSeparator)
            {
                tmpStr[1] = tmpStr[1].Substring(1, tmpStr[1].Length - 1);
            }

            // create a new path object with the two path strings appended together.
            return new Path(tmpStr[0] + tmpStr[1], lhs.PathSeparator);
        }

        /// <summary>
        /// Compare a path to a path string.
        /// </summary>
        /// <param name="lhs">The path</param>
        /// <param name="rhs">The path string.  Must use the same path separator as the lhs</param>
        /// <returns></returns>
	    public static bool operator == (Path lhs, string rhs)
        {
            var rhsPath = new Path(rhs, lhs.PathSeparator);
            return lhs == rhsPath;
        }

        /// <summary>
        /// Not Equal operator
        /// </summary>
        /// <param name="lhs">The path</param>
        /// <param name="rhs">The path string.  Must use the same path separator as the lhs</param>
        /// <returns></returns>
	    public static bool operator != (Path lhs, string rhs)
        {
            return !(lhs == rhs);
        }

        /// <summary>
        /// Add 2 paths together
        /// </summary>
        /// <param name="lhs">The path</param>
        /// <param name="rhs">The path string.  Must use the same path separator as the lhs</param>
        /// <returns></returns>
        public static Path operator + (Path lhs, string rhs)
        {
            var rhsPath = new Path(rhs, lhs.PathSeparator);
            return lhs + rhsPath;
        }

        /// <summary>
        /// Append the specified rhs paths to the current path (modifying the current path)
        /// </summary>
        /// <param name="rhs">1 or more rhs paths to append to this path</param>
        /// <returns>this</returns>
        public Path Append(params string[] rhs)
        {
            string rhsStr = string.Empty;

            foreach (var path in rhs)
            {
                rhsStr += (path + this.PathSeparator);
            }

            var newPath = this.HasTrailingPathSeparator ?
                            (this + rhsStr).WithTrailingPathSeparator :
                            (this + rhsStr).WithoutTrailingPathSeparator;

            this._path = newPath._path;
            this._drive = newPath._drive;

            // need to reinitialise after mutating
            InitialiseLazies();

            return this;
        }

        /// <summary>
        /// Append the specified rhs paths to the current path (modifying the current path)
        /// </summary>
        /// <param name="rhs">1 or more rhs paths to append to this path</param>
        public Path Append(params Path[] rhs)
        {
            var newPath = new Path(this);

            foreach (var path in rhs)
            {
                newPath += path;
            }

            if (this.HasTrailingPathSeparator != newPath.HasTrailingPathSeparator)
            {
                newPath = this.HasTrailingPathSeparator ?
                            newPath.WithTrailingPathSeparator :
                            newPath.WithoutTrailingPathSeparator;
            }

            this._path = newPath._path;
            this._drive = newPath._drive;

            // need to reinitialise after mutating
            InitialiseLazies();

            return this;
        }

        /// <summary>
        /// <para>
        /// Return the parent at the specfied path depth of the current path.
        /// </para>
        /// <b>Note:</b> Like the Depth property:
        /// <list type="bullet">
        ///     <item>If the path was created WITH      a leading path separator, then the root path (ie. / or c:\) resides at depth 0.</item>
        ///     <item>If the path was created WITHOUT   a leading path separator, then the first named parent resides at depth 0</item>
        /// </list>
        /// </summary>
        /// <param name="depth"></param>
        /// <returns>The path of the parent at the specified depth</returns>
        public Path GetParentAtDepth(UInt32 depth)
        {
            if (!this.IsValid
                || depth >= this.Depth)
            {
                return this;
            }

            // create Path object to parent object at depth N relative to this
            // path object.
            bool hasLeadingPS = this.HasLeadingPathSeparator;

            // reset m_sPath as required.
            var tmpPath = hasLeadingPS ? $"{this.PathSeparator}" : string.Empty;

            // split path into it tokens.
            var tokens = this._path.Split(this.PathSeparator);

            for (int i = hasLeadingPS ? 1 : 0; i < tokens.Length && depth > 0; i++)
            {
                if (tmpPath.Length > 0 && tmpPath[tmpPath.Length - 1] != this.PathSeparator)
                {
                    tmpPath += this.PathSeparator;
                }

                tmpPath += tokens[i];
                --depth;
            }

            // put / on end as required
            if (tmpPath.Length > 0 ? (tmpPath[tmpPath.Length - 1] != this.PathSeparator) : true)
            {
                tmpPath += this.PathSeparator;
            }

            return new Path(this._drive + tmpPath, this.PathSeparator);
        }

        /// <summary>
        /// Return the path to the deepest common ancestor between this and the rhs path
        /// </summary>
        /// <param name="rhs">The other path</param>
        /// <returns>The path to the deepest common ancestor</returns>
        public Path GetDeepestCommonAncestor(Path rhs)
        {
            var lhs = this;
            var lhsDepth = lhs.Depth;
            var rhsDepth = rhs.Depth;
            var minDepth = Math.Min(lhsDepth, rhsDepth);

            for (uint i = 0; i <= minDepth; i++)
            {
                var lhsParent = lhs.GetParentAtDepth(i);
                var rhsParent = rhs.GetParentAtDepth(i);

                if (lhsParent != rhsParent)
                {
                    // lhs & rhs don't match anymore!  therefore the previous index must be the last time that
                    // they did.
                    return (i == 0) ? CreateEmptyPath() : GetParentAtDepth(i - 1);
                }
            }

            return this.GetParentAtDepth(minDepth);
        }

        /// <summary>
        /// Return the path to the deepest common ancestor between this and the rhs path
        /// </summary>
        /// <param name="rhs">The other path</param>
        /// <returns>The path to the deepest common ancestor</returns>
        public Path GetDeepestCommonAncestor(string rhs)
        {
            return GetDeepestCommonAncestor(new Path(rhs, this.PathSeparator));
        }

        /// <summary>
        /// What path is requried to travel from the tip of this path to the tip of the rhs path?  The paths must share a common parent.
        /// </summary>
        /// <param name="rhs">The other path</param>
        /// <returns>The relative path between this and the rhs path</returns>
        public Path GetRelativePathTo(Path rhs)
        {
            if (!this.IsValid)
            {
                return CreateEmptyPath();
            }

            if (!rhs.IsValid)
            {
                return CreateEmptyPath();
            }

            if (this.Drive.ToLower() != rhs.Drive.ToLower())
            {
                // paths have differnt root's (if they are specified)
                return CreateEmptyPath();
            }

            var deepestCommonAncestor = GetDeepestCommonAncestor(rhs);

            if (!deepestCommonAncestor.IsValid)
            {
                // no common ancestor found!
                return CreateEmptyPath();
            }

            if (deepestCommonAncestor == this)
            {
                System.Diagnostics.Debug.Assert(IsParentOf(rhs));

                // this path must be a direct ancestor of the rhs, so just return the rhs
                return new Path(rhs);
            }

            // start to create the return path by building the path requried
            // to travel from the tip of this path to the common ancestor

            var depthOfDCA = deepestCommonAncestor.Depth;
            var relativePath = string.Empty;

            for (uint i = this.Depth; i > depthOfDCA; i--)
            {
                relativePath += (".." + this.PathSeparator);
            }

            if (depthOfDCA == rhs.Depth)
            {
                // looks like the rhs path is an ancestor of this path, so just return now!
                return this + new Path(relativePath, this.PathSeparator);
            }

            for (uint i = depthOfDCA + 1; i <= rhs.Depth; i++)
            {
                relativePath += (rhs.GetParentAtDepth(i).BaseName + this.PathSeparator);
            }

            var retValue = this + new Path(relativePath, this.PathSeparator);

            return rhs.HasTrailingPathSeparator
                        ? retValue.WithTrailingPathSeparator
                        : retValue;
        }

        /// <summary>
        /// What path is requried to travel from the tip of this path to the tip of the rhs path?  The paths must share a common parent.
        /// </summary>
        /// <param name="rhs">The other path</param>
        /// <returns>The relative path between this and the rhs path</returns>
        public Path GetRelativePathTo(string rhs)
        {
            return GetRelativePathTo(new Path(rhs, this.PathSeparator));
        }

        /// <summary>
        /// Test for equality
        /// </summary>
        /// <param name="rhs">The other path</param>
        /// <param name="caseInsensitive">case insensitive equality test?</param>
        /// <returns>true if the same, otherwise false</returns>
        public bool Equals(Path rhs, bool caseInsensitive)
        {
            if (!this.IsValid && !rhs.IsValid)
            {
                // both paths are invalid!
                return true;
            }

            if (this.IsValid != rhs.IsValid)
            {
                // one of the paths is valid, whilst the other is not!
                return false;
            }

            if (this.PathSeparator != rhs.PathSeparator)
            {
                // mismatching path separators!
                return false;
            }

            string[] tmpStr =
            {
                // ALWAYS make drive letter lower case as drive letter are case insensitive during comparisons
                // make path lower case if case insensitive comparison is needed
			    this._drive.ToLower() + (caseInsensitive ? this._path.ToLower() : this._path),
                rhs._drive.ToLower() + (caseInsensitive ? rhs._path.ToLower() : rhs._path)
            };

            // make sure both paths have a / on the end.
            if (tmpStr[0][tmpStr[0].Length - 1] != this.PathSeparator)
            {
                tmpStr[0] += this.PathSeparator;
            }

            if (tmpStr[1][tmpStr[1].Length - 1] != this.PathSeparator)
            {
                tmpStr[1] += this.PathSeparator;
            }

            return tmpStr[0] == tmpStr[1];
        }

        /// <summary>
        /// Test for equality
        /// </summary>
        /// <param name="rhs">The other path</param>
        /// <param name="caseInsensitive">case insensitive equality test?</param>
        /// <returns>true if the same, otherwise false</returns>
        public bool Equals(string rhs, bool caseInsensitive)
        {
            return Equals(new Path(rhs, this.PathSeparator), caseInsensitive);
        }

        /// <summary>
        /// Custom version of Object.Equals
        /// </summary>
        /// <param name="obj">The other path</param>
        /// <returns>true if the same, otherwise false</returns>
        public override bool Equals(object obj)
        {
            return (obj is Path) ? this == (obj as Path) : false;
        }

        /// <summary>
        /// Custom version of Object.GetHashCode
        /// </summary>
        /// <returns>The hash of the path</returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Custom version of Object.ToString
        /// </summary>
        /// <returns>The stringified version of the path</returns>
        public override string ToString()
        {
            return (this._drive ?? string.Empty) + (this._path ?? string.Empty);
        }
    }

    
    /// <summary>
    /// Support functions
    /// </summary>
    public partial class Path
    {
        /// <summary>
        /// Initialise the more expensive operation that are implemented using lazies so they don't imped performance until the first time they are required
        /// </summary>
        private void InitialiseLazies()
        {
            this._isLocal       = new Lazy<bool>    (() => IsLocalPath());
            this._isUrl         = new Lazy<bool>    (() => IsUrlPath());
            this._isRelative    = new Lazy<bool>    (() => IsRelativePath());
            this._isUnc         = new Lazy<bool>    (() => IsUncPath());
            this._localPath     = new Lazy<Path>    (() => ToLocalPath());
            this._parentPath    = new Lazy<Path>    (() => GetParentPath());
            this._baseName      = new Lazy<string>  (() => GetBaseName());
            this._baseTitle     = new Lazy<string>  (() => GetBaseTitle());
            this._baseExtension = new Lazy<string>  (() => GetBaseExtension());
            this._depth         = new Lazy<uint>    (() => GetDepth());
        }

        /// <summary>
        /// Create the default empty path
        /// </summary>
        /// <returns></returns>
        private Path CreateEmptyPath()
        {
            return new Path(string.Empty, this.PathSeparator);
        }

        /// <summary>
        /// Initialise with a specific path and path separator
        /// </summary>
	    private void InitialiseWithPath(string path, char pathSeparator)
        {
            // extract the drive the path resides on...
            var regEx = new Regex("^([a-zA-Z]):");

            if (regEx.IsMatch(path ?? string.Empty))
            {
                this._pathSeparator = pathSeparator;
                this._drive = path.Substring(0, 2);
                this._path = path.Substring(2, path.Length - 2);
            }
            else
            {
                this._pathSeparator = pathSeparator;
                this._drive = string.Empty;
                this._path = path;
            }
        }

        /// <summary>
        /// Set the path separator
        /// </summary>
        /// <param name="value"></param>
        /// <exception cref="FormatException"></exception>
        /// <exception cref="ApplicationException"></exception>
        private void SetPathSeparator(char value)
        {
            if (value != System.IO.Path.DirectorySeparatorChar && value != System.IO.Path.AltDirectorySeparatorChar)
                throw new FormatException($"Separator must be wither {System.IO.Path.DirectorySeparatorChar} or {System.IO.Path.AltDirectorySeparatorChar}!");

            // early bail if path separator already set as required
            if (this._pathSeparator == value)
                return;

            // check if path already has the "new" value, if it does then we can't update to the new path separator character because it would mess up the path
            if ((this._path?.IndexOf(value) ?? -1) >= 0)
                throw new ApplicationException($"Cannot update {this._path} to use the new path separator character {value} because it is already using that character!");

            // only need to actually update the path if it has a value...
            if (string.IsNullOrEmpty(this._path) == true)
            {
                this._pathSeparator = value;
            }
            else
            {
                // update path string with new separator character and retain the new path separator charcter
                this._path = this._path.Replace(this._pathSeparator, value);
                this._pathSeparator = value;
            }
        }

        /// <summary>
        /// Get the path depth
        /// </summary>
        /// <returns></returns>
        private uint GetDepth()
        {
            if (!this.IsValid)
            {
                return 0;
            }

            var length = this._path.Length;
            var depth = (uint)0;

            for (var i = 0; i < length; i++)
            {
                if (this._path[i] == this.PathSeparator)
                {
                    ++depth;
                }
            }

            if (depth > 0
                && length > 0
                && this._path[length - 1] == this.PathSeparator)
            {
                // PATH_SEPARATOR_CHAR on the end, reduce count by 1
                --depth;
            }

            return depth;
        }

        /// <summary>
        /// Get the tail filename/path extension
        /// </summary>
        /// <returns></returns>
        private string GetBaseExtension()
        {
            if (!this.IsValid)
            {
                return string.Empty;
            }

            var tmpStr = this.BaseName;

            // make sure there is no / on the end.
            if (tmpStr[tmpStr.Length - 1] == this.PathSeparator)
            {
                tmpStr = tmpStr.Substring(0, tmpStr.Length - 1);
            }

            var pos = tmpStr.LastIndexOf('.');

            if (pos != -1)
            {
                // extract extension.
                return tmpStr.Substring(pos + 1, tmpStr.Length - (pos + 1));
            }
            else
            {
                // no extension!
                return string.Empty;
            }
        }

        //
        private string GetBaseTitle()
        {
            if (!this.IsValid)
            {
                return string.Empty;
            }

            var tmpStr = this.BaseName;
            var pos = tmpStr.LastIndexOf('.');

            if (pos != -1)
            {
                // remove extension.
                tmpStr = tmpStr.Substring(0, pos);
            }

            return tmpStr;
        }

        private string GetBaseName()
        {
            if (!this.IsValid)
            {
                return string.Empty;
            }

            // reverse find a / ignoring the end / if it exists.	
            var length = this._path.Length;
            var offset = ((this._path[length - 1] == this.PathSeparator && length - 2 > 0) ? length - 2 : length - 1);
            var pos = this._path.LastIndexOf(this.PathSeparator, offset);

            // extract filename given position of find / and return it.
            if (pos != -1)
            {
                return this._path.Substring(pos + 1, length - (pos + 1) - (offset != (length - 1) ? 1 : 0));
            }
            else
            {
                return this._path;
            }
        }

        private Path GetParentPath()
        {
            if (!this.IsValid || this.IsRoot)
            {
                // return empty path.
                return CreateEmptyPath();
            }

            // reverse find a / ignoring the end / if it exists.
            var length = this._path.Length;
            var offset = ((this._path[length - 1] == this.PathSeparator && length - 2 > 0) ? length - 2 : length - 1);
            var pos = this._path.LastIndexOf(this.PathSeparator, offset);

            // create new path object given position of find / and return it.
            if (pos != -1)
            {
                return new Path(this._drive + this._path.Substring(0, pos + 1), this.PathSeparator);
            }
            else
            {
                // not parent path avaliable, return an empty path.
                return CreateEmptyPath();
            }
        }

        private Path ToLocalPath()
        {
            // Uri.TryCreate does NOT support calling Uri.LocalPath is path is relative :-(
            if (this.IsRelative)
            {
                var retValue = new Path(this);

                retValue.PathSeparator = System.IO.Path.DirectorySeparatorChar;

                return retValue;
            }

            // NOT relative, so parse as absolute
            if (Uri.TryCreate(this.ToString(), UriKind.Absolute, out var uri) == false)
                throw new FormatException($"Path {this.ToString()} cannot be parsed to a local, unc, url (absolute) path!");

            if (true
                && uri.IsAbsoluteUri == true
                && uri.IsFile == true
                && uri.IsUnc == false)
            {
                return new Path(uri.LocalPath, System.IO.Path.DirectorySeparatorChar);
            }
            else if (true
                     && uri.IsAbsoluteUri == true
                     && uri.IsFile == true
                     && uri.IsUnc == true)
            {
                return new Path(uri.LocalPath, System.IO.Path.DirectorySeparatorChar);
            }
            else if (true
                     && uri.IsAbsoluteUri == true
                     && uri.IsFile == false
                     && uri.IsUnc == false
                     && (uri.Scheme == "http" || uri.Scheme == "https"))
            {
                return new Path(uri.ToString(), '/');
            }
            else
            {
                throw new FormatException($"Path {this.ToString()} cannot be parsed to a local, unc, url or relative path!");
            }
        }

        private bool IsUncPath()
        {
            return true
                    && this.IsValid == true
                    && Uri.TryCreate(this.ToString(), UriKind.Absolute, out var uri) == true
                    && uri is null == false
                    && uri.IsFile == true
                    && uri.IsUnc == true;
        }

        private bool IsRelativePath()
        {
            if (this.IsValid == false)
                return false;

            // need to test for unc or local first because it seems relative has precedence :-(
            {
                if (Uri.TryCreate(this.ToString(), UriKind.Absolute, out var uri) == true && uri is null == false)
                {
                    // check for unc
                    if (true
                        && uri.IsFile == true
                        && uri.IsUnc == true)
                    {
                        return false;
                    }

                    // check for local
                    if (true
                        && uri.IsFile == true
                        && uri.IsUnc == false)
                    {
                        return false;
                    }
                }
            }

            // relative test
            {
                return true
                        && (Uri.TryCreate(this.ToString(), UriKind.Relative, out var uri) == true)
                        && uri is null == false
                        && uri.IsAbsoluteUri == false;
            }
        }

        private bool IsUrlPath()
        {
            return true
                    && this.IsValid == true
                    && Uri.TryCreate(this.ToString(), UriKind.Absolute, out var uri) == true
                    && uri is null == false
                    && uri.IsFile == false
                    && uri.IsUnc == false
                    && (uri.Scheme == "http" || uri.Scheme == "https");
        }

        private bool IsLocalPath()
        {
            return true
                    && this.IsValid == true
                    && Uri.TryCreate(this.ToString(), UriKind.Absolute, out var uri) == true
                    && uri is null == false
                    && uri.IsFile == true
                    && uri.IsUnc == false;
        }
    }
}
