﻿using System;

namespace FileSystem.Utilities
{
    /// <summary>
    /// Extension methods for <see cref="String"/> type that support the <see cref="Path"/> type.
    /// </summary>
    static public class StringExtensionsForPath
    {
        /// <summary>
        /// Have a good guess at what the appropriate directory path separator should be for the given path
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static char GetPathSeparator(this string path)
        {
            if (string.IsNullOrWhiteSpace(path))
                return System.IO.Path.DirectorySeparatorChar;

            // not much we can do except return the uri as a path
            var hasDefDirSepChars = path.IndexOf(System.IO.Path.DirectorySeparatorChar) >= 0;
            var hasAltDirSepChars = path.IndexOf(System.IO.Path.AltDirectorySeparatorChar) >= 0;

            // 11
            if (hasDefDirSepChars == true && hasAltDirSepChars == true)
            {
                return System.IO.Path.DirectorySeparatorChar;
            }
            // 10
            else if (hasDefDirSepChars == true && hasAltDirSepChars == false)
            {
                return System.IO.Path.DirectorySeparatorChar;
            }
            // 01
            else if (hasDefDirSepChars == false && hasAltDirSepChars == true)
            {
                return System.IO.Path.AltDirectorySeparatorChar;
            }
            // 00
            // else if (hasDefDirSepChars == false && hasAltDirSepChars == false)
            {
                return System.IO.Path.DirectorySeparatorChar;
            }
        }
    }
}
