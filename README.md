# FileSystem.Utilities.Path #

This is a port of my handy C++ file system path utility class that I [submitted to code project](https://www.codeproject.com/Articles/10265/FileSystem-path-string-wrapper-class) over 10 years ago!  The repo has in it a simple console application (along with a solution file) that simply runs the happy path tests embedded with the FileSystem.Utilities.Path.

The solution & projects are for VS2015, but I'm pretty sure I've not used any C# 6 specific syntax or .NET 4.6 specific libraries (haven't tried to "downgrade" though and test) so the class should be usable in projects that target older versions of the .NET framework (and C# language).

The code speaks for itself.  Feel free to send me pull requests!

Cheers