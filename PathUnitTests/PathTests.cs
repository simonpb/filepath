﻿using System;
using FileSystem.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace PathUnitTests
{
    [TestClass]
    public class PathTests
    {
        [TestMethod]
        [TestCategory("Unit")]
        public void PathTests_HappyPaths()
        {
            // equivalence, depth & base name tests
            {
                Path path1 = new Path("C:\\a\\");
                Path path2 = new Path("c:\\a\\");


                Assert.IsTrue(path1.IsValid);
                Assert.IsTrue(path2.IsValid);

                Assert.IsTrue(path1 == path2);
                Assert.IsTrue((path1 != path2) == false);

                Assert.IsTrue(path1.Depth == path2.Depth);
                Assert.IsTrue(path1.Depth == 1);

                Assert.AreEqual("a", path1.BaseName);
                Assert.AreEqual("", path1.BaseExtension);
                Assert.AreEqual("a", path1.BaseTitle);

                Assert.IsTrue(path1.HasTrailingPathSeparator);
                Assert.AreEqual("C:\\a", path1.WithoutTrailingPathSeparator.ToString());

                Assert.AreEqual("C", path1.Drive);
                Assert.AreEqual("c", path2.Drive);
            }

            // equivalence, depth & base name tests
            {
                Path path1 = new Path("C:\\a\\");
                Path path2 = new Path("c:\\a");

                Assert.IsTrue(path1.IsValid);
                Assert.IsTrue(path2.IsValid);

                Assert.IsTrue(path1 == path2);
                Assert.IsTrue((path1 != path2) == false);

                Assert.IsTrue(path1.Depth == path2.Depth);
                Assert.IsTrue(path1.Depth == 1);

                Assert.AreEqual("a", path1.BaseName);
                Assert.AreEqual("", path1.BaseExtension);
                Assert.AreEqual("a", path1.BaseTitle);

                Assert.IsTrue(path1.HasTrailingPathSeparator);
                Assert.AreEqual("C:\\a", path1.WithoutTrailingPathSeparator.ToString());

                Assert.AreEqual("C", path1.Drive);
                Assert.AreEqual("c", path2.Drive);
            }

            // equivalence, depth & base name tests
            {
                var path1Str = "c:\\a\\b\\c\\d.txt\\";
                var path2Str = "c:\\a\\b\\c\\d.txt";

                Path path1 = new Path(path1Str, path1Str.GetPathSeparator());
                Path path2 = new Path(path2Str, path2Str.GetPathSeparator());

                Assert.IsTrue(path1.IsValid);
                Assert.IsTrue(path2.IsValid);

                Assert.IsTrue(path1 == path2);
                Assert.IsTrue((path1 != path2) == false);

                Assert.IsTrue(path1.Depth == path2.Depth);
                Assert.IsTrue(path1.Depth == 4);

                Assert.AreEqual("d.txt", path1.BaseName);
                Assert.AreEqual("txt", path1.BaseExtension);
                Assert.AreEqual("d", path1.BaseTitle);

                Assert.AreEqual("c", path1.Drive);
                Assert.AreEqual("c", path2.Drive);
            }

            // operator +/+= (Path), drive letter & equivalence tests
            {
                var path1Str = "c:\\a\\b";
                var path2Str = "c\\d.txt";
                var path3Str = "c:\\a\\b\\c\\d.txt";

                Path path1 = new Path(path1Str, path1Str.GetPathSeparator());
                Path path2 = new Path(path2Str, path2Str.GetPathSeparator());
                Path path3 = new Path(path3Str, path3Str.GetPathSeparator());
                Path path4 = path1 + path2;
                Path path5 = path1; path5 += path2;

                Assert.IsTrue(path1.IsValid);
                Assert.IsTrue(path2.IsValid);
                Assert.IsTrue(path3.IsValid);
                Assert.IsTrue(path4.IsValid);
                Assert.IsTrue(path5.IsValid);

                Assert.IsTrue(path4 == path5 && path4 == path3 && path5 == path3);
                Assert.IsTrue(((path1 + path2) != path3) == false);

                Assert.IsTrue(path4.Depth == path3.Depth);
                Assert.IsTrue(path4.Depth == 4);

                Assert.IsTrue(path5.Depth == path3.Depth);
                Assert.IsTrue(path5.Depth == 4);

                Assert.AreEqual("d.txt", path4.BaseName);
                Assert.AreEqual("txt", path4.BaseExtension);
                Assert.AreEqual("d", path4.BaseTitle);

                Assert.AreEqual("d.txt", path5.BaseName);
                Assert.AreEqual("txt", path5.BaseExtension);
                Assert.AreEqual("d", path5.BaseTitle);

                Assert.AreEqual("c", path4.Drive);
                Assert.AreEqual("c", path5.Drive);
            }

            // operator +/+= (string), drive letter & equivalence tests
            {
                var path1Str = "c:\\a\\b";
                var path2Str = "c\\d.txt";
                var path3Str = "c:\\a\\b\\c\\d.txt";

                Path path1 = new Path(path1Str, path1Str.GetPathSeparator());
                Path path2 = new Path(path2Str, path2Str.GetPathSeparator());
                Path path3 = new Path(path3Str, path3Str.GetPathSeparator());
                Path path4 = path1 + path2Str;
                Path path5 = path1; path5 += path2Str;

                Assert.IsTrue(path1.IsValid);
                Assert.IsTrue(path2.IsValid);
                Assert.IsTrue(path3.IsValid);
                Assert.IsTrue(path4.IsValid);
                Assert.IsTrue(path5.IsValid);

                Assert.IsTrue(path4 == path5 && path4 == path3 && path5 == path3);
                Assert.IsTrue(((path1 + path2) != path3) == false);

                Assert.IsTrue(path4.Depth == path3.Depth);
                Assert.IsTrue(path4.Depth == 4);

                Assert.IsTrue(path5.Depth == path3.Depth);
                Assert.IsTrue(path5.Depth == 4);

                Assert.AreEqual("d.txt", path4.BaseName);
                Assert.AreEqual("txt", path4.BaseExtension);
                Assert.AreEqual("d", path4.BaseTitle);

                Assert.AreEqual("d.txt", path5.BaseName);
                Assert.AreEqual("txt", path5.BaseExtension);
                Assert.AreEqual("d", path5.BaseTitle);

                Assert.AreEqual("c", path4.Drive);
                Assert.AreEqual("c", path5.Drive);
            }

            // depth tests
            {
                var path1 = new Path("1/2/3/4/5/6/7", '/');
                var path2 = new Path("/1/2/3/4/5/6/7", '/');

                var path3 = new Path("1/2/3/4/5/6/7/", '/');
                var path4 = new Path("/1/2/3/4/5/6/7/", '/');

                Assert.IsTrue(path1.Depth == 6);
                Assert.IsTrue(path2.Depth == 7);

                Assert.IsTrue(path3.Depth == 6);
                Assert.IsTrue(path4.Depth == 7);
            }

            // depth tests
            {
                // note the missing / at the start!
                var path1 = new Path("1/2/3/4/5/6/7", '/');
                var path2 = new Path("/1/2/3/4", '/');

                Assert.IsTrue(!path2.IsParentOf(path1));
                Assert.IsTrue(!path1.IsChildOf(path2));

                Assert.IsTrue(path1.Depth == 6);

            }

            // deep depth tests
            {
                Path path1 = new Path("/1/2/3/4/5/6/7/8/9/10/11/12/13/14", '/');

                for (int i = 1; i < path1.Depth; i++)
                {
                    var pathAtN = path1.GetParentAtDepth((uint)i);
                    var baseFilename = pathAtN.BaseName;

                    Assert.IsTrue(int.Parse(baseFilename) == i);
                    Assert.IsTrue(pathAtN.Parent == path1.GetParentAtDepth((uint)i - 1));
                }


                Assert.IsTrue(string.IsNullOrEmpty(path1.Drive));
            }

            // is child/parent of tests
            {
                var path1 = new Path("/1/2/3/4/5/6/7", '/');
                var path2 = new Path("/1/2/3/4", '/');

                Assert.IsTrue(path2.IsParentOf(path1));
                Assert.IsTrue(path1.IsChildOf(path2));

                Assert.IsTrue(path2.IsParentOf(path1.ToString()));
                Assert.IsTrue(path1.IsChildOf(path2.ToString()));
            }

            // trailing separator tests
            {
                var path1 = new Path("/1/2/3/4/5/6/7", '/');
                var path2 = new Path("/1/2/3/4/5/6/7/", '/');

                Assert.IsTrue(path1.WithoutTrailingPathSeparator == path1);
                Assert.IsTrue(path1.WithTrailingPathSeparator == path2);
                Assert.IsTrue(path2.WithoutTrailingPathSeparator == path1);
                Assert.IsTrue(path2.WithTrailingPathSeparator == path2);

                Assert.IsTrue(!path1.WithoutTrailingPathSeparator.HasTrailingPathSeparator);
                Assert.IsTrue(path1.WithTrailingPathSeparator.HasTrailingPathSeparator);
                Assert.IsTrue(!path2.WithoutTrailingPathSeparator.HasTrailingPathSeparator);
                Assert.IsTrue(path2.WithTrailingPathSeparator.HasTrailingPathSeparator);
            }

            // leading separator tests
            {
                var path1 = new Path("1/2/3/4/5/6/7/", '/');
                var path2 = new Path("/1/2/3/4/5/6/7/", '/');

                Assert.IsTrue(path1.WithoutLeadingPathSeparator == path1);
                Assert.IsTrue(path1.WithLeadingPathSeparator == path2);
                Assert.IsTrue(path2.WithoutLeadingPathSeparator == path1);
                Assert.IsTrue(path2.WithLeadingPathSeparator == path2);

                Assert.IsTrue(!path1.WithoutLeadingPathSeparator.HasLeadingPathSeparator);
                Assert.IsTrue(path1.WithLeadingPathSeparator.HasLeadingPathSeparator);
                Assert.IsTrue(!path2.WithoutLeadingPathSeparator.HasLeadingPathSeparator);
                Assert.IsTrue(path2.WithLeadingPathSeparator.HasLeadingPathSeparator);
            }

            // is root tests
            {
                var path1 = new Path("/", '/');
                var path2 = new Path("c:\\");
                var path3 = new Path("c:\\1\\2\\3\\4");

                Assert.IsTrue(path1.IsRoot);
                Assert.IsTrue(path2.IsRoot);
                Assert.IsTrue(path3.GetParentAtDepth(0).IsRoot);
                Assert.IsTrue(!path3.GetParentAtDepth(1).IsRoot);


                var path4 = new Path("c:");
                var path5 = new Path("");

                Assert.IsTrue(!path4.IsRoot);
                Assert.IsTrue(!path5.IsRoot);
            }

            // leading / trailing path separator tests
            {
                var path1 = new Path("/1/2/3/4/5/6/7/", '/');
                var path2 = new Path("/1/2/3/4/5/6/7", '/');
                var path3 = new Path("1/2/3/4/5/6/7/", '/');
                var path4 = new Path("1/2/3/4/5/6/7", '/');

                Assert.IsTrue(path1.HasLeadingPathSeparator && path1.HasTrailingPathSeparator);
                Assert.IsTrue(path2.HasLeadingPathSeparator && !path2.HasTrailingPathSeparator);
                Assert.IsTrue(!path3.HasLeadingPathSeparator && path3.HasTrailingPathSeparator);
                Assert.IsTrue(!path4.HasLeadingPathSeparator && !path4.HasTrailingPathSeparator);
            }

            // relative path tests 1
            {
                var path1 = new Path("/1/2/3/", '/');
                var path2 = new Path("/1/2/3/4/5/6/7/8/9", '/');
                var path3 = new Path("/1/2/3/a/b/c/d/e/f/g", '/');

                var path12 = path1.GetRelativePathTo(path2);
                var path21 = path2.GetRelativePathTo(path1);
                var path23 = path2.GetRelativePathTo(path3);

                Assert.IsTrue(path12 == path2);
                Assert.IsTrue(path21 == path2 + new Path("../../../../../../", '/'));
                Assert.IsTrue(path23 == path2 + new Path("../../../../../../a/b/c/d/e/f/g", '/'));
            }

            // relative path tests - 2
            {
                var path1 = new Path("/d/User Data/Simon/Documents/Visual Studio 2015/Projects/filepath/Path/obj", '/');
                var path2 = new Path("/d/User Data/Simon/Documents/Visual Studio 2015/Projects/MVCSuccinctly (2)/packages/WebGrease.1.5.2", '/');
                var path3 = new Path("/d/User Data/Simon/Documents/Visual Studio 2015/Projects/filepath/Path/obj/../../../MVCSuccinctly (2)/packages/WebGrease.1.5.2", '/');

                var path12 = path1.GetRelativePathTo(path2);
                Assert.IsTrue(path12 == path3);
            }

            // ellipsoid path tests
            {
                var path1 = new Path("/1/2/3/../../../", '/');
                var path2 = new Path("/a/b/c/1/2/3/../../../d/e/f/", '/');
                var path3 = new Path("/1/2/3/a/b/c/../../../x/y/y/../../../4/5/6/", '/');

                Assert.IsTrue(path1.Clean == "/");
                Assert.IsTrue(path2.Clean == "/a/b/c/d/e/f/");
                Assert.IsTrue(path3.Clean == "/1/2/3/4/5/6/");
            }

            // deepest common ancestor tests
            {
                var path1 = new Path("/1/2/3/", '/');
                var path2 = new Path("/1/2/3/4/5/6/", '/');
                var path3 = new Path("/1/2/3/a/b/c/", '/');
                var path4 = new Path("/a/b/c/d");

                Assert.IsTrue(path1.GetDeepestCommonAncestor(path1) == path1);
                Assert.IsTrue(path1.GetDeepestCommonAncestor(path2) == path1);
                Assert.IsTrue(path1.GetDeepestCommonAncestor(path3) == path1);
                Assert.IsTrue(path1.GetDeepestCommonAncestor(path4).IsValid == false);
            }

            // Append test 1
            {
                var path1 = new Path("/", '/');
                var listDirectories = new List<string>();

                for (int i = 1; i <= 10; i++)
                {
                    listDirectories.Add(i.ToString());
                }

                path1.Append(listDirectories.ToArray());

                for (int i = 1; i < path1.Depth; i++)
                {
                    var pathAtN = path1.GetParentAtDepth((uint)i).WithoutTrailingPathSeparator;
                    var baseFilename = pathAtN.BaseName;

                    Assert.IsTrue(int.Parse(baseFilename) == i);
                    Assert.IsTrue(pathAtN.Parent == path1.GetParentAtDepth((uint)i - 1));
                }
            }

            // Append test 2
            {
                var path1 = new Path("/", '/');
                var listDirectories = new List<string>();

                for (int i = 1; i <= 10; i++)
                {
                    listDirectories.Add(i.ToString());
                }

                path1.Append("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");

                for (int i = 1; i < path1.Depth; i++)
                {
                    var pathAtN = path1.GetParentAtDepth((uint)i).WithoutTrailingPathSeparator;
                    var baseFilename = pathAtN.BaseName;

                    Assert.IsTrue(int.Parse(baseFilename) == i);
                    Assert.IsTrue(pathAtN.Parent == path1.GetParentAtDepth((uint)i - 1));
                }
            }

            // Append test 3
            {
                var path1 = new Path("/", '/');
                var listDirectories = new List<Path>();

                for (int i = 1; i <= 10; i++)
                {
                    listDirectories.Add(new Path(i.ToString(), '/'));
                }

                path1.Append(listDirectories.ToArray());

                for (int i = 1; i < path1.Depth; i++)
                {
                    var pathAtN = path1.GetParentAtDepth((uint)i).WithoutTrailingPathSeparator;
                    var baseFilename = pathAtN.BaseName;

                    Assert.IsTrue(int.Parse(baseFilename) == i);
                    Assert.IsTrue(pathAtN.Parent == path1.GetParentAtDepth((uint)i - 1));
                }
            }

            // Append test 4
            {
                var path1 = new Path("/", '/');
                var listDirectories = new List<string>();

                for (int i = 1; i <= 10; i++)
                {
                    listDirectories.Add(i.ToString());
                }

                path1.Append(
                    new Path("1", '/'),
                    new Path("2", '/'),
                    new Path("3", '/'),
                    new Path("4", '/'),
                    new Path("5", '/'),
                    new Path("6", '/'),
                    new Path("7", '/'),
                    new Path("8", '/'),
                    new Path("9", '/'),
                    new Path("10", '/')
                    );

                for (int i = 1; i < path1.Depth; i++)
                {
                    var pathAtN = path1.GetParentAtDepth((uint)i).WithoutTrailingPathSeparator;
                    var baseFilename = pathAtN.BaseName;

                    Assert.IsTrue(int.Parse(baseFilename) == i);
                    Assert.IsTrue(pathAtN.Parent == path1.GetParentAtDepth((uint)i - 1));
                }
            }

            // String extension tests
            {
                var path1Str = "/1/2/3/4";
                var path2Str = "\\1\\2\\3\\4";
                var path3Str = "/1/2\\3\\4/5/6";

                Assert.IsTrue(path1Str.GetPathSeparator() == '/');
                Assert.IsTrue(path2Str.GetPathSeparator() == '\\');
                Assert.IsTrue(path3Str.GetPathSeparator() == System.IO.Path.DirectorySeparatorChar);
            }

            // path separator update tests
            {
                var path1Str = "/1/2/3/4";
                var path2Str = "\\1\\2\\3\\4";
                var path3Str = "/1/2\\3\\4/5/6";

                // should be ok to set the path separator to the original value
                {
                    var path1 = new Path(path1Str, path1Str.GetPathSeparator());
                    var path2 = new Path(path2Str, path2Str.GetPathSeparator());
                    var path3 = new Path(path3Str, path3Str.GetPathSeparator());

                    path1.PathSeparator = '/';
                    path2.PathSeparator = '\\';
                    path3.PathSeparator = '\\';

                    Assert.IsTrue(path1.PathSeparator == '/');
                    Assert.IsTrue(path2.PathSeparator == '\\');
                    Assert.IsTrue(path3.PathSeparator == '\\');
                }

                // Now try to really update the path separator
                {
                    var path1 = new Path(path1Str, path1Str.GetPathSeparator());
                    var path2 = new Path(path2Str, path2Str.GetPathSeparator());
                    var path3 = new Path(path3Str, path3Str.GetPathSeparator());

                    path1.PathSeparator = '\\';
                    path2.PathSeparator = '/';

                    Assert.IsTrue(path1.PathSeparator == '\\');
                    Assert.IsTrue(path2.PathSeparator == '/');

                    // update of path3 should fail
                    try
                    {
                        path3.PathSeparator = '/';

                        Assert.Fail();
                    }
                    catch
                    {
                        // exception thrown, then all good (I hope)
                        Assert.IsTrue(path3.PathSeparator == '\\');
                    }
                }

                // update to invalid path char should fail
                {
                    var path1 = new Path(path1Str, path1Str.GetPathSeparator());
                    var path2 = new Path(path2Str, path2Str.GetPathSeparator());
                    var path3 = new Path(path3Str, path3Str.GetPathSeparator());

                    try
                    {
                        path1.PathSeparator = ':';

                        Assert.Fail();
                    }
                    catch
                    {
                        // exception thrown, then all good (I hope)
                        Assert.IsTrue(path1.PathSeparator == '/');
                    }
                }
            }

            // path local, unc, url, relative tests
            {   
                // relative
                var relPath1Str = "/a/b/c/d";
                var relPath2Str = "\\a\\b\\c\\d";

                // unc
                var uncPath1Str = "//a/b/c/d";
                var uncPath2Str = "\\\\a\\b\\c\\d";

                // local
                var localPath1Str = "c:/a/b/c/d";
                var localPath2Str = "c:\\a\\b\\c\\d";

                // url
                var urlPath1Str = "http://foobar.com.au/a/b/c/d";
                var urlPath2Str = "https://foobar.com.au/a/b/c/d";

                var relPath1 = new Path(relPath1Str, '/');
                var relPath2 = new Path(relPath2Str, '\\');
                var uncPath1 = new Path(uncPath1Str, '/');
                var uncPath2 = new Path(uncPath2Str, '\\');
                var localPath1 = new Path(localPath1Str, '/');
                var localPath2 = new Path(localPath2Str, '\\');
                var urlPath1 = new Path(urlPath1Str, '/');
                var urlPath2 = new Path(urlPath2Str, '/');

                {
                    Assert.IsTrue (relPath1.IsRelative);
                    Assert.IsFalse(relPath1.IsUnc);
                    Assert.IsFalse(relPath1.IsLocal);
                    Assert.IsFalse(relPath1.IsUrl);

                    Assert.IsTrue (relPath2.IsRelative);
                    Assert.IsFalse(relPath2.IsUnc);
                    Assert.IsFalse(relPath2.IsLocal);
                    Assert.IsFalse(relPath2.IsUrl);

                    Assert.IsTrue(relPath1.LocalPath == relPath2Str);
                    Assert.IsTrue(relPath2.LocalPath == relPath2Str);
                }

                {
                    Assert.IsFalse(uncPath1.IsRelative);
                    Assert.IsTrue (uncPath1.IsUnc);
                    Assert.IsFalse(uncPath1.IsLocal);
                    Assert.IsFalse(uncPath1.IsUrl);

                    Assert.IsFalse(uncPath2.IsRelative);
                    Assert.IsTrue (uncPath2.IsUnc);
                    Assert.IsFalse(uncPath2.IsLocal);
                    Assert.IsFalse(uncPath2.IsUrl);

                    Assert.IsTrue(uncPath1.LocalPath == uncPath2Str);
                    Assert.IsTrue(uncPath2.LocalPath == uncPath2Str);
                }

                {
                    Assert.IsFalse(localPath1.IsRelative);
                    Assert.IsFalse(localPath1.IsUnc);
                    Assert.IsTrue (localPath1.IsLocal);
                    Assert.IsFalse(localPath1.IsUrl);

                    Assert.IsFalse(localPath2.IsRelative);
                    Assert.IsFalse(localPath2.IsUnc);
                    Assert.IsTrue (localPath2.IsLocal);
                    Assert.IsFalse(localPath2.IsUrl);

                    Assert.IsTrue(localPath1.LocalPath == localPath2);
                    Assert.IsTrue(localPath2.LocalPath == localPath2);
                }

                {
                    Assert.IsFalse(urlPath1.IsRelative);
                    Assert.IsFalse(urlPath1.IsUnc);
                    Assert.IsFalse(urlPath1.IsLocal);
                    Assert.IsTrue (urlPath1.IsUrl);

                    Assert.IsFalse(urlPath2.IsRelative);
                    Assert.IsFalse(urlPath2.IsUnc);
                    Assert.IsFalse(urlPath2.IsLocal);
                    Assert.IsTrue (urlPath2.IsUrl);

                    Assert.IsTrue(urlPath1.LocalPath == urlPath1);
                    Assert.IsTrue(urlPath2.LocalPath == urlPath2);
                }
            }
        }
    }
}
